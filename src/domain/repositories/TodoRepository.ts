import { Todo } from "../entities/Todo";

export interface TodoRepository {
  GetTodos(): Promise<Todo[]>;
  AddTodo(data:Todo): void;
  DeleteTodo(data:Todo): void;
  UpdateTodo(data:Todo): void;
  CompleteTodo(data:Todo): void;

}
