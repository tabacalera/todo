import { Todo } from "../entities/Todo"
import { TodoRepository } from "../repositories/TodoRepository"

export interface TodoService {
    GetTodos(): Promise<Todo[]>;
    AddTodo(data:Todo): void;
    DeleteTodo(data:Todo): void;
    UpdateTodo(data:Todo): void;
    CompleteTodo(data:Todo): void;

}

export default class TodoServiceImpl implements TodoService {
    todoRepo: TodoRepository

    constructor(ir: TodoRepository){
        this.todoRepo = ir;
    }

    async GetTodos(): Promise<Todo[]> {
        return this.todoRepo.GetTodos();
      }

    async AddTodo(data:Todo) {

        if(data.title.length===0){
            throw "Can't be empty"
        }
        else {
            this.todoRepo.AddTodo(data)
        }
    }
    async DeleteTodo(data:Todo) {

        if(data.completed){
            throw "Can't delete incomplete Todo"
        }else {

            this.todoRepo.DeleteTodo(data)
        }
        
    }
    async UpdateTodo(data:Todo) {

        this.todoRepo.UpdateTodo(data)
        
    }
    async CompleteTodo(data:Todo) {
        this.todoRepo.CompleteTodo(data)
    }
}