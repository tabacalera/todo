import { ADD_TODO,LIST_LOAD_REQUEST, LIST_LOAD_SUCCESS, LIST_LOAD_FAILURE } from "./Item.types"

const initialState = [
    {
        text:"",
        id:0
    }
]
function items(state = initialState, action = null) {
    switch (action.type) {
        case LIST_LOAD_REQUEST:
            return {
                ...state,
                loading: true,
            }

        case LIST_LOAD_FAILURE:
            return {
                ...state,
                loading: false,
            }

        case LIST_LOAD_SUCCESS:
            return {
                ...state,
                items: action.payload,
                loading: false,
            }
        
        case ADD_TODO:
            return[
                ...state,
                {
                    id: state.reduce((maxId, todo) => Math.max(todo.id,maxId), -1) +1,
                    text: action.payload
                }
            ]

        default:
            return state
    }
}
export default items
