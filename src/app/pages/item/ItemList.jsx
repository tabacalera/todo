import React from "react";
import { connect } from "react-redux";
import { refreshList,addTodo } from "./Item.actions";

const ItemList = ({ items, refreshList, addTodo }) => {
  const [input,setInput] = React.useState("")
  
  const handleSubmit=(event)=>{
    event.preventDefault()
    addTodo(input)
  }
  console.log(items)
  return(
    <div>
      <form onSubmit={handleSubmit}>
        <input type="text" onChange={(e)=> setInput(e.target.value)}></input>
        <button type="submit">Submit</button>
      </form>
      <ul>
        {items.map(item => 
          item.id===0 ?null:
          <li key={item.id}>{item.text}</li>
        )
        }
      </ul>
    </div>
  );
}

const mapStateToProps = state => ({
  items: state.items
});

const mapDispatchToProps = dispatch => ({
  refreshList: () => dispatch(refreshList),
  addTodo: (text)=> dispatch(addTodo(text))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemList);
