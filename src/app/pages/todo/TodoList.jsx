import React from "react";
import { connect } from "react-redux";
import { deleteTodo, refreshList,updateTodo, completeTodo } from "../todo/todo.actions";
import { addTodo } from "./todo.actions";

const TodoList = ({ todos, addTodo, deleteTodo, updateTodo, completeTodo }) => {
  const [input,setInput] = React.useState("")
  
  React.useEffect(()=>{
      addTodo()
  }, [])

  // handle add todo
  const handleSubmit=(event)=>{
    event.preventDefault()
    setInput("")
    addTodo(input)
  }

  // handle edit todoasxdas2
  const handleChange=(value, id)=>{
    const todoItems = todos
    todoItems.map(item=>{
        if(item.id===id){
            item.title=value
            updateTodo(item)
        }
    })
}
  return(
    <div >
      <div style={{ flex: 1, flexDirection: "row"}}>
      <form onSubmit={handleSubmit}>
        <input type="text" value={input} onChange={(e)=> setInput(e.target.value)}></input>
        <button onSubmit={handleSubmit}>Submit</button>
      </form>
        <br />
        {todos.map(item => 
          item.id===0 ?null:
          (<>
            <div style={{flex: 1, flexDirection:"row"}}>
                <input style={{textDecorationLine: `${item.completed?"line-through": "none" }`}} type="text" value={item.title} onChange={(e)=>handleChange(e.target.value, item.id)}/>
                <button onClick={()=>deleteTodo(item)} >Delete</button>
                <button onClick={()=>completeTodo(item)} >Completed</button>
            </div>
          </>)
        )}
      </div>
     
    </div>
  );
}

const mapStateToProps = state => ({
    todos: state.todos.todos
});

const mapDispatchToProps = dispatch => ({
  addTodo: (text)=> addTodo(text).then(dispatch(refreshList)),
  updateTodo: (todo)=> updateTodo(todo).then(dispatch(refreshList)),
  deleteTodo: (todo)=> deleteTodo(todo).then(dispatch(refreshList)),
  completeTodo: (todo)=> completeTodo(todo).then(dispatch(refreshList))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
