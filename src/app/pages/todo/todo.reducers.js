import { ADD_TODO, LIST_LOAD_REQUEST, LIST_LOAD_SUCCESS, LIST_LOAD_FAILURE } from "./todo.types"


const initialState = {
    loading: false,
    todos:[]
}
function todos(state = initialState, action = null) {
    switch (action.type) {
      
        case LIST_LOAD_REQUEST:
            return {
                ...state,
                loading: true,
            }

        case LIST_LOAD_FAILURE:
            return {
                ...state,
                loading: false,
            }

        case LIST_LOAD_SUCCESS:
            return {
                ...state,
                todos: action.payload,
                loading: false,
            }

        case ADD_TODO:
            return Object.assign({}, state, {
               
                todos: state.todos.concat(action.payload),
                
            })
        

        default:
            return state
    }
}
export default todos
