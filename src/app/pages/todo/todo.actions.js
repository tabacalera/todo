import { ADD_TODO,LIST_LOAD_REQUEST, LIST_LOAD_SUCCESS, LIST_LOAD_FAILURE } from "./todo.types"

import TodoServiceImpl from "../../../domain/usecases/TodoService"
import TodoRepositoryLocalStorageImpl from "../../../data/repositories/TodoRepositoryLocalStorageImpl"

export const refreshList = async (dispatch) => {
    dispatch({ type: LIST_LOAD_REQUEST })

    try {
        const todoRepos = new TodoRepositoryLocalStorageImpl()
        const todoService = new TodoServiceImpl(todoRepos)
        const todos = await todoService.GetTodos()
        dispatch({ type: LIST_LOAD_SUCCESS, payload: todos })
    } catch (error) {
        dispatch({ type: LIST_LOAD_FAILURE, error })
    }
}
//hard coded ID
let nextId= 3

export const addTodo = async payload => {
    
    if(payload===undefined) return
    
    try{
        
        const title = payload
        payload = { id: nextId++, title, editing: false }
        const todoRepos = new TodoRepositoryLocalStorageImpl()
        const todoService = new TodoServiceImpl(todoRepos)
        await todoService.AddTodo(payload)
        return { type: ADD_TODO, payload}
    } catch (error) {
        alert(error)
    }
}

export const deleteTodo = async todo=> {

    try {
        const todoRepos = new TodoRepositoryLocalStorageImpl()
        const todoService = new TodoServiceImpl(todoRepos)
        await todoService.DeleteTodo(todo)
    } catch (error) {
        alert(error)
    }
}
export const updateTodo = async todo=> {
    try {
        const todoRepos = new TodoRepositoryLocalStorageImpl()
        const todoService = new TodoServiceImpl(todoRepos)
        await todoService.UpdateTodo(todo)
    } catch (error) {
        alert(error)
    }
}
export const completeTodo = async todo=> {
    try {
        const todoRepos = new TodoRepositoryLocalStorageImpl()
        const todoService = new TodoServiceImpl(todoRepos)
        await todoService.CompleteTodo(todo)
    } catch (error) {
        alert(error)
    }
}