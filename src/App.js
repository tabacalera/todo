import React from "react"
import { Provider } from "react-redux"
import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from "redux-thunk"

import ItemList from "./app/pages/item/ItemList"


import TodoList from "./app/pages/todo/TodoList"
import todos from "./app/pages/todo/todo.reducers"

// Setup Redux store with Thunks
const reducers = combineReducers({ todos })
const store = createStore(reducers, applyMiddleware(thunk))
// let todo = [{
//     "id": 1,
//     "title": "First Todos",
//     "completed": false
// },
// {
//     "id": 2,
//     "title": "Second Todo",
//     "completed": false
// }]

const App = () => {
    // React.useEffect(()=>{
    //     const jsonString  = JSON.stringify(todo)
    //     localStorage.setItem("todos", jsonString)
    
    // }, [])
    return(
        <Provider store={store}>
            <TodoList />
        </Provider>
    )}

export default App
