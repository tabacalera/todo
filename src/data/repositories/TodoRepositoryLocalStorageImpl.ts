import { Todo } from "../../domain/entities/Todo"
import { TodoRepository } from "../../domain/repositories/TodoRepository"

class TodoDTO {
    id: number= 0;
    title: string = "";
    completed: boolean= false;
}

export default class TodoRepositoryLocalStorageImpl implements TodoRepository {
    async GetTodos(): Promise<Todo[]> {
        
        var jsonString = localStorage.getItem("todos")
        let objTodo
        if(jsonString!== null){
            objTodo = JSON.parse(jsonString)
        }
        return objTodo.map((todoItem:TodoDTO)=> new Todo(todoItem.id, todoItem.title, todoItem.completed))
        
        

    }
    async AddTodo(data:Todo){
        console.log(data);
        
        const jsonString = localStorage.getItem("todos")
        let objTodo

        if(jsonString!== null){
            objTodo = JSON.parse(jsonString)
        }
        objTodo.push(data)
        console.log(objTodo);
        var stringTodo = JSON.stringify(objTodo)
        localStorage.setItem("todos", stringTodo)
        
    }

    async DeleteTodo(data:Todo){
        var intData = parseInt(data.id.toString())

        const jsonString = localStorage.getItem("todos")
        let objTodo
        if(jsonString!== null){
            objTodo = JSON.parse(jsonString)
        }
        for (let i = 0; i < objTodo.length; i++) {
            if (intData === objTodo[i].id){
                objTodo.splice(i,1);
              break;
            }
        }
        var stringTodo = JSON.stringify(objTodo)
        localStorage.setItem("todos", stringTodo)
    }
    async UpdateTodo(data:Todo){
        var id = parseInt(data.id.toString())

        const jsonString = localStorage.getItem("todos")
        let objTodo

        if(jsonString!== null){
            objTodo = JSON.parse(jsonString)
        }

        for (let i = 0; i < objTodo.length; i++) {
            if (id === objTodo[i].id){
                objTodo[i].title = data.title
              break;
            }
        }

        var stringTodo = JSON.stringify(objTodo)
        localStorage.setItem("todos", stringTodo)
    }
    async CompleteTodo(data:Todo) {
        var id = parseInt(data.id.toString())

        const jsonString = localStorage.getItem("todos")
        let objTodo

        if(jsonString!== null){
            objTodo = JSON.parse(jsonString)
        }
        for (let i = 0; i < objTodo.length; i++) {
            if (id === objTodo[i].id){
                objTodo[i].completed = true
              break;
            }
        }
        var stringTodo = JSON.stringify(objTodo)
        localStorage.setItem("todos", stringTodo)
    }
    
}