import { Todo } from "../../domain/entities/Todo"
import { TodoRepository } from "../../domain/repositories/TodoRepository"

class TodoDTO {
    id: number= 0;
    title: string = "";
    completed: boolean= false;
}

let todos = [{
    "id": 1,
    "title": "First Todo",
    "completed": false
},
{
    "id": 2,
    "title": "Second Todo",
    "completed": false
}]

export default class TodoRepositoryArrayImpl implements TodoRepository {
    async GetTodos(): Promise<Todo[]> {
        var jsonString=JSON.stringify(todos)
        var res = JSON.parse(jsonString)
        return res.map((todoItem:TodoDTO)=> new Todo(todoItem.id, todoItem.title, todoItem.completed))
    }
    async AddTodo(data:Todo){
        todos.push(data)
    }
    async DeleteTodo(data:Todo){
        var intData = parseInt(data.id.toString())
        for (let i = 0; i < todos.length; i++) {
            if (intData === todos[i].id){
                todos.splice(i,1);
              break;
            }
          }
    }
    async UpdateTodo(data:Todo){
        console.log(data.title)
        var id = parseInt(data.id.toString())
        for (let i = 0; i < todos.length; i++) {
            if (id === todos[i].id){
                todos[i].title = data.title
              break;
            }
          }
    }
    async CompleteTodo(data:Todo){
        console.log(data.title)
        var id = parseInt(data.id.toString())
        for (let i = 0; i < todos.length; i++) {
            if (id === todos[i].id){
                todos[i].completed = true
              break;
            }
          }
    }
}